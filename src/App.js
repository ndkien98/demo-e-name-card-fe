import './App.css';
import FooterComponent from "./components/FooterComponent";
import HeaderComponent from "./components/HeaderComponent";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import React from "react";
import ENameCardView from "./components/ENameCardView";
import ENameCardCreate from "./components/ENameCardCreate";
import {ENameCardDetail} from "./components/ENameCardDetail";
function App() {
    return (
        <div>
            <HeaderComponent/>
            <Router>
                <div className="container">
                    <Switch>
                        <Route path = "/" exact component = {ENameCardView}></Route>
                        <Route path = "/name-cards" component = {ENameCardView}></Route>
                        <Route path = "/name-card/add" component = {ENameCardCreate}></Route>
                        <Route path = "/name-card/detail/:id" component = {ENameCardDetail}></Route>
                    </Switch>
                </div>
            </Router>
            <FooterComponent/>
        </div>
    );
}

export default App;
