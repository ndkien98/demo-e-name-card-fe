import React from 'react';
import '../App.css'
import ENameCardService from "../service/ENameCardService";
import avatar from '../assets/avatar.png'

class ENameCardCreate extends React.Component{

    constructor(props) {
        super();

        this.state = {
            avatar: '',
            email: '',
            faceBookLink: '',
            fullName: '',
            phone: '',
            positions: ''
        }
    }

    handleChange = (e) => {
        const {name, value} = e.target
        this.setState({[name]: value})
    }



    handleSubmit = (e) => {
        e.preventDefault()
        console.log(this.state)
        ENameCardService.createEmployee(this.state).then((res) => {
            alert("create e-name card successfully")
            this.props.history.push('/name-cards')
        })
    }

    render() {

        return(
            <div>
                <form className="modal-content" onSubmit={this.handleSubmit}>
                    <div>
                        <div className="container">
                            <div className="avatar-div">
                                <img src={avatar} style={{"width":"200px" ,"height":"200px"}}/>
                                <label>Chọn ảnh đại diện: </label>
                                <input type="file"/>
                            </div>
                        </div>
                        <div className="content">
                            <div className="left">
                                <label htmlFor="fullName"><b>Họ tên:</b></label>
                                <input type="text" value={this.state.fullName} onChange={this.handleChange} placeholder="Nhập họ tên" name="fullName"/>

                                <label htmlFor="phone"><b>SĐT:</b></label>
                                <input type="text" value={this.state.phone} onChange={this.handleChange} placeholder="Nhập số điện thoại" name="phone"/>

                                <label htmlFor="email"><b>Email:</b></label>
                                <input type="text" value={this.state.email} onChange={this.handleChange} placeholder="Nhập email" name="email"/>
                            </div>
                            <div className="right">
                                <label htmlFor="faceBookLink"><b>Facebook Link:</b></label>
                                <input type="text" value={this.state.faceBookLink} onChange={this.handleChange} placeholder="Nhập số chức vụ" name="faceBookLink" />

                                <label htmlFor="positions"><b>Chức vụ:</b></label>
                                <input type="text" value={this.state.positions} onChange={this.handleChange} placeholder="Nhập số chức vụ" name="positions" />
                            </div>
                        </div>

                        <div className="center">
                            <button type="submit" className="btn btn-success ">Tạo</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default ENameCardCreate;
