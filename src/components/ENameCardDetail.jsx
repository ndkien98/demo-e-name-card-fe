import React from 'react';
import ENameCardService from "../service/ENameCardService";
import "./ENameCardDetail.css"
import avatar from "../assets/avatar.png";
import {images} from "../images";



export class ENameCardDetail extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            nameCard: {},
            qrCode:'',
            byteArrayQR: {}
        }
        this.viewDetail(this.state.id)


        this.viewQRCode(this.state.id);
    }

    viewQRCode(id) {
        ENameCardService.downloadQRCode(id).then((res) => {
            this.setState({byteArrayQR: res.data})
            const base64 = btoa(
                new Uint8Array(res.data)
                    .reduce((data, byte) => data + String.fromCharCode(byte), '')
            )
            this.setState({qrCode: "data:image/png;base64," + base64})
        })
    }

    viewDetail(id){
        ENameCardService.viewDetail(id).then((res) => {
            this.setState({nameCard : res.data.data})
            console.log(this.state.nameCard)
        })
    }

    downloadQRCode(){
        const url = window.URL.createObjectURL(new Blob([this.state.byteArrayQR]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'qr.png');
        document.body.appendChild(link);
        link.click();
    }

    redirectFB = () => {
        console.log("click link fb")
        window.open('fb://profile?id=minkxtb')
    }

    redirectZalo = () => {
        console.log("click link zalo")
        window.open('https://zalo.me/0984945109')
    }

    render() {
        const {avatarE,email,faceBookLink,fullName,id,phone,positions} = this.state.nameCard

        const linkAPK = "fb://facewebmodal/f?href=https://www.facebook.com/nguyet.chuppy.1";
        const linkIOS = "fb://profile?id=bon.trainer"
        const linkZalo = "https://zalo.me/0984945109"

        return(
            <div>
                <div className="modal-content">
                    <div>
                        <div className="container">
                            <div className="row">
                                <div className="column-layout" >
                                    <div className="row-d">
                                        <div className="column-d">
                                            <div className="row-d">
                                                <div className="column-d">
                                                    <img src={avatar} style={{"width":"200px" ,"height":"200px"}}/>
                                                    <label style={{"margin-left": "25%"}}>Ảnh cá nhân </label>
                                                </div>
                                                <div className="column-d">
                                                    <div style={{"margin-top": "30%"}}>
                                                        <h3>{fullName}</h3>
                                                        <label>Chức vụ : {positions}</label>
                                                    </div>
                                                    {/*<a href={linkAPK}>Link fb APK </a>*/}
                                                    {/*<a href={linkIOS}>Link fb IOS</a>*/}
                                                    {/*<a href={linkZalo}>Link Zalo</a>*/}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="column-d">
                                            <div>
                                                <img src={this.state.qrCode} style={{"width":"200px" ,"height":"200px"}}/>
                                                <div className="qr-i">
                                                    <img src={images[`share.png`].default} width={40} height={40}/>
                                                    <img src={images[`download.png`].default} width={40} height={40} onClick={() => this.downloadQRCode()}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row-d">
                                        <div className="column-d">
                                            <div className="column-d">
                                                <div className="element">
                                                    <img src={images[`phone.png`].default} width={40} height={40}/>
                                                    <label>{phone}</label>
                                                </div>
                                            </div>
                                            <div className="column-d">
                                                <div className="element">
                                                    <img src={images[`mail.png`].default} width={40} height={40}/>
                                                    <label>{email}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="column-d">
                                            <div className="element-m">
                                                <label>Mạng xã hội: </label>
                                                <div>
                                                    <img src={images[`zalo.png`].default} width={40} height={40} onClick={this.redirectZalo}/>
                                                </div>
                                                <div>
                                                    <img src={images[`fb.jpg`].default} width={40} height={40} onClick={this.redirectFB}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

