import React, {Component} from 'react';
import ENameCardService from "../service/ENameCardService";
import iconAdd from "../assets/add.png"
import iconExport from "../assets/export.png"

class ENameCardView extends Component {
    constructor(props) {
        super(props)

        this.state = {
            ENameCards: [],
            file: ''
        }
    }

    onFileChange = (event) => {
        this.setState({ file : event.target.files[0] })
    };

    viewDetail = (id) => {
        this.props.history.push("/name-card/detail/" + id)
    }

    uploadFile = () => {
        ENameCardService.uploadFile(this.state.file).then((res) => {
            console.log(res)
            alert("import file successfully")
        })
    }

    downloadFile = () =>{
        ENameCardService.downloadFile().then((res) => {
            const url = window.URL.createObjectURL(new Blob([res.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'template.xlsx');
            document.body.appendChild(link);
            link.click();
        })
    }

    componentDidMount() {
        const page =  0
        const size = 20
        ENameCardService.getENameCard(page,size).then((res) => {
            this.setState({ENameCards: res.data.data.content});
            console.log(this.state)
        });
    }

    render() {
        return (
            <div>
                <div>
                    <table>
                        <tr>
                            <td>
                                <div className="b-add">
                                <img className="iconOne" src={iconAdd} onClick={() => this.props.history.push('/name-card/add')}/>
                                <label>Thêm mới</label>
                            </div>
                            </td>
                            <td style={{"width":"50%"}}>
                                <div className="b-add">
                                    {/*<img className="iconPage" src={iconAddPage} onClick={() => this.props.history.push('/name-card/_add')}/>*/}
                                    <input type='file' name='file' onChange={this.onFileChange} />
                                    <button className="btn btn-success" onClick={this.uploadFile}> Thêm mới với batch</button>
                                </div>
                            </td>
                            <td>
                                <div className="b-add">
                                    <img className="iconOne" src={iconExport} onClick={this.downloadFile}/>
                                    <label style={{"margin-left": "10px"}}>Tải mẫu</label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <h2 className="text-center">ENameCards</h2>
                <br></br>
                <div className="row">
                    <table className="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>fullName</th>
                            <th>phone</th>
                            <th>email</th>
                            <th>faceBookLink</th>
                            <th>positions</th>
                            <th>action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.ENameCards.map(
                                eNameCard =>
                                        <tr>
                                        <td style={{"width": "15%"}}> {eNameCard.fullName}</td>
                                        <td> {eNameCard.phone}</td>
                                        <td> {eNameCard.email}</td>
                                        <td> {eNameCard.faceBookLink}</td>
                                        <td> {eNameCard.positions}</td>
                                        <td style={{"width": "20%"}}>
                                            <div>
                                                <div className="column">
                                                    <button onClick={() => this.viewDetail(eNameCard.id)}
                                                            className="btn btn-info">View
                                                    </button>
                                                </div>
                                                <div className="column">
                                                    <button style={{marginLeft: "10px"}}
                                                            onClick={() => this.deleteEmployee(eNameCard.id)}
                                                            className="btn btn-danger">Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>

                </div>

            </div>
        )
    }
}

export default ENameCardView;
