import React, {Component} from 'react';

class HeaderComponent extends Component {

    constructor(props) {
        super(props);
    }

    backHome(){
        this.props.history.push("/name-cards")
    }

    render() {
        return (
            <div>
                <header>
                    <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                        <div><p className="navbar-brand" onClick={this.backHome}>E-NameCard </p></div>
                    </nav>
                </header>
            </div>
        );
    }
}

export default HeaderComponent;
