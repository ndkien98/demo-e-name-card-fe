import axios from 'axios';


const E_NAME_CARD_API_BASE_URL = "http://localhost:8080/e-name-card";

class ENameCardService {

    getENameCard(page,size){
        return axios.get(E_NAME_CARD_API_BASE_URL+ "?page=" + page +  "&" + "size=" + size);
    }

    createEmployee(eNameCard){
        return axios.post(E_NAME_CARD_API_BASE_URL, eNameCard);
    }

    uploadFile(file){
        const formData = new FormData();
        console.log("file" ,file);
        formData.append("file",file,file.name)
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        return axios.post(E_NAME_CARD_API_BASE_URL + "/batch" ,formData,config)
    }

    downloadFile() {
        return axios.get(E_NAME_CARD_API_BASE_URL + "/excel",{
            headers:
                {
                    'Content-Disposition': "attachment; filename=template.xlsx",
                    'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                },
            responseType: 'arraybuffer',
        });
    }

    viewDetail(id) {
        return axios.get(E_NAME_CARD_API_BASE_URL + "/" + id);
    }

    downloadQRCode(id){
        return axios.get(E_NAME_CARD_API_BASE_URL + "/qr-code/" + id,{
            headers:
                {
                    'Content-Disposition': "attachment; filename=template.xlsx",
                    'Content-Type': 'image/png'
                },
            responseType: 'arraybuffer',
        });
    }

}

export default new ENameCardService()
